Pod::Spec.new do |s|
  s.summary = "Puppeteer Pod"
  s.homepage = "https://github.com/TitouanVanBelle/Puppeteer/"
  s.authors = "Titouan Van Belle"
  s.license = { :type => 'Copyright', :file => './LICENSE.md' }

  s.name = 'Puppeteer'
  s.version = '1.0'
  s.platform = :ios
  s.ios.deployment_target = '7.0'
  s.prefix_header_file = 'Puppeteer/Puppeteer-Prefix.pch'
  s.source_files = 'Sources/*.{h,m}'
  s.requires_arc = true
  s.source = {
    :git => 'https://github.com/TitouanVanBelle/Puppeteer.git',
    :tag => '1.0'
  }

  s.dependency 'AFNetworking'
  s.dependency 'SSZipArchive'

end
