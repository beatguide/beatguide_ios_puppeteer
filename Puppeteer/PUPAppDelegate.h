//
//  PUPAppDelegate.h
//  Puppeteer
//
//  Created by Tito on 09/04/2014.
//  Copyright (c) 2014 Puppeteer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PUPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
