//
//  PUPMainViewController.h
//  Puppeteer
//
//  Created by Tito on 09/04/2014.
//  Copyright (c) 2014 Puppeteer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Puppeteer.h"
#import "PUPNetwork.h"

@interface PUPMainViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@end
