//
//  PUPMainViewController.m
//  Puppeteer
//
//  Created by Tito on 09/04/2014.
//  Copyright (c) 2014 Puppeteer. All rights reserved.
//

#import "PUPMainViewController.h"

NSString * const MyUserDefaultsIdentifier = @"PUPUserDefaultsIdentifier";
NSString * const MyServerVersionUrl = @"https://bitbucket.org/beatguide/beatguide_ios_puppeteer/raw/master/PUPServer/response.json";
NSString * const MyServerBundleUrl = @"https://bitbucket.org/beatguide/beatguide_ios_puppeteer/raw/master/PUPServer/PUPBundle.bundle";

@implementation PUPMainViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // only neccessary for testing
    [[Puppeteer sharedInstance] deleteBundle];
    
    // listen for changes
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(insertStringsIntoUI)
                                                 name:PUPNofictionIdentifier
                                               object:nil];
    // load UI
    [self insertStringsIntoUI];
}


- (void)insertStringsIntoUI
{
    [self.textField setText:PULocalizedString(@"Something that is not there", nil)];
    [self.label setText:PULocalizedString(@"Done", nil)];
    [self.button setTitle:PULocalizedString(@"News", nil) forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(buttonTapped) forControlEvents:UIControlEventTouchUpInside];
}

-(void)buttonTapped
{
    if (YES) {
        [[Puppeteer sharedInstance] setBundleWithRemoteUrl:[NSURL URLWithString:MyServerBundleUrl]];
    }
    
    else{
        [self getStringsIfLaterVersionAvailable];
    }
}

/**
 *  Uses the PUPNetwork example to fetch he latest version of the bundle
 */
- (void)getStringsIfLaterVersionAvailable
{
    // get currenttly stored version
    NSString* currentVersion = [[NSUserDefaults standardUserDefaults] objectForKey:MyUserDefaultsIdentifier];
    
    // create an instance of the PUPNetwork
    PUPNetwork* pupNetwork = [[PUPNetwork alloc] init];

    // get the latst version
    [pupNetwork getLatestVersion:MyServerVersionUrl
                  currentVersion:currentVersion
               completionHandler:^(NSString *version, NSString *remoteBundleUrl) {
                   
                   // is this version newer
                   BOOL isNewVersionAvailable = ([version compare:currentVersion options:NSNumericSearch] == NSOrderedDescending);
        
                   if (isNewVersionAvailable) {
                       
                       NSLog(@"Puppeteer: New Strings Found");
                       
                       // get the new bundle
                       [pupNetwork getBundle:remoteBundleUrl version:version completionHandler:^(BOOL success, NSString *version, NSString *remoteBundleUrl) {
                           
                           // store new version
                           [[NSUserDefaults standardUserDefaults] setObject:version forKey:MyUserDefaultsIdentifier];
                           [[NSUserDefaults standardUserDefaults] synchronize];
                           
                           // set the new bundle on the Puppeteer
                           [[Puppeteer sharedInstance] setBundleWithTempFilePath:remoteBundleUrl];
                       }];
                   }
    }];

}

@end
