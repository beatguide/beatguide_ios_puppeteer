//
//  PUPNetwork.h
//  Puppeteer
//
//  Created by Brendon Blackwell on 23/04/14.
//  Copyright (c) 2014 Puppeteer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "SSZipArchive.h"

@interface PUPNetwork : NSObject



#pragma mark - Network functions

/**
 *  Gets a json file from a git repo
 *  Simulating a JSON server
 *
 *  @param serverUrl      The remote url of a json file
 *  @param currentVersion The current local bundle version
 *  @param callback       The completion block of the request
 */
- (void)getLatestVersion:(NSString *)serverUrl
          currentVersion:(NSString *)currentVersion
       completionHandler:(void (^)(NSString *version, NSString *remoteBundleUrl))callback;



/**
 *  Gets a .bundle file from a git repo
 *  Simulating a file server
 *
 *  @param fileUrl  Url of the remote .bundle file
 *  @param version  Version of the bundle file attemping to be retrieved
 *  @param callback The completion block of the request
 */
- (void)getBundle:(NSString *)fileUrl
          version:(NSString *)version
completionHandler:(void (^)(BOOL success, NSString *version, NSString *remoteBundleUrl))callback;



#pragma mark - Helper function

/**
 *  Helper function to unzip a .zip
 *  Depends on SSZipArchive
 */
- (BOOL)unzipFrom:(NSString*)source
               to:(NSString*)destination;

@end
