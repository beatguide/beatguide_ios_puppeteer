//
//  PUPNetwork.m
//  Puppeteer
//
//  Created by Brendon Blackwell on 23/04/14.
//  Copyright (c) 2014 Puppeteer. All rights reserved.
//

#import "PUPNetwork.h"

@implementation PUPNetwork


#pragma mark - Remote Version Control Method

- (void)getLatestVersion:(NSString *)serverUrl
          currentVersion:(NSString *)currentVersion
       completionHandler:(void (^)(NSString *version, NSString *remoteBundleUrl))callback
{    
    // set up AFNetworking
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];

    // Fire request
    [manager GET:serverUrl
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSDictionary* result = responseObject[@"result"];
             callback(result[@"version"], result[@"fileUrl"]);
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             callback(currentVersion, nil);
         }];
}


#pragma mark - Remote Download Method

- (void)getBundle:(NSString *)fileUrl
          version:(NSString *)version
completionHandler:(void (^)(BOOL success, NSString *version, NSString *remoteBundleUrl))callback
{
    // where new strings will be downloaded & unzipped to
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    NSURL *tmpZippedFilePath = [tmpDirURL URLByAppendingPathComponent:@"myTmpBundle.zip"];
    NSURL *tmpUnzippedFilePath = [tmpDirURL URLByAppendingPathComponent:@"myTmpBundle"];
    
    // set up AFNetworking for request
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:fileUrl]];
    AFURLConnectionOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.outputStream = [NSOutputStream outputStreamWithURL:tmpZippedFilePath append:NO];
    
    // unzip
    [operation setCompletionBlock:^{
        BOOL result = [self unzipFrom:[tmpZippedFilePath absoluteString] to:[tmpUnzippedFilePath absoluteString]];
        NSString* path = (result) ? [tmpUnzippedFilePath absoluteString] : nil;
        callback(result, version, path);
    }];
    
    // fire download & unzip
    [operation start];
}



#pragma mark - Unzip Methods

- (BOOL)unzipFrom:(NSString*)source to:(NSString*)destination
{
    // zip to temp folder
    NSError *error = nil;
    [SSZipArchive unzipFileAtPath:source toDestination:destination
                        overwrite:YES
                         password:nil
                            error:&error];
    
    // remove zipped version
    [[NSFileManager defaultManager] removeItemAtPath:source error:nil];
    
    return (!error);
}


@end
