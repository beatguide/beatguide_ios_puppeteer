//
//  PUPMacros.h
//  Puppeteer
//
//  Created by Brendon Blackwell on 24/04/14.
//  Copyright (c) 2014 Puppeteer. All rights reserved.
//

#define PULocalizedString(key, comment) \
[Puppeteer localizedStringForKey:(key) value:@"" table:nil bundle:nil]
#define PULocalizedStringFromTable(key, tbl, comment) \
[Puppeteer localizedStringForKey:(key) value:@"" table:(tbl) bundle:nil]
#define PULocalizedStringFromTableInBundle(key, tbl, bundle, comment) \
[Puppeteer localizedStringForKey:(key) value:@"" table:(tbl) bundle:(bundle)]
#define PULocalizedStringWithDefaultValue(key, tbl, bundle, val, comment) \
[Puppeteer localizedStringForKey:(key) value:(val) table:(tbl) bundle:(bundle)]
