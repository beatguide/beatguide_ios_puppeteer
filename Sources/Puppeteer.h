//
//  Puppeteer.h
//  Puppeteer
//
//  Created by Tito on 09/04/2014.
//  Copyright (c) 2014 Puppeteer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PUPMacros.h"

extern NSString * const PUPNofictionIdentifier;
extern NSString * const PUPBundleIdentifier;

@interface Puppeteer : NSObject

+ (Puppeteer *)sharedInstance;
+ (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName bundle:(NSBundle *)bundle;
- (void)setBundleWithRemoteUrl:(NSURL *)url;
- (void)setBundleWithRemoteUrl:(NSURL *)url completionHandler:(void (^)(NSError *error))callback;
- (void)setBundleWithTempFilePath:(NSString *)filePath;
- (void)setBundleWithTempFilePath:(NSString *)filePath completionHandler:(void (^)(NSError *error))callback;

- (BOOL)deleteBundle;
+ (void)log:(BOOL)isLogging;


@end