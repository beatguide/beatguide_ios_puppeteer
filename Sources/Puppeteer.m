//
//  Puppeteer.m
//  Puppeteer
//
//  Created by Tito on 09/04/2014.
//  Copyright (c) 2014 Puppeteer. All rights reserved.
//

NSString * const PUPNofictionIdentifier = @"PUPNofictionIdentifier";
NSString * const PUPBundleIdentifier = @"PUPBundle.bundle";
NSString * const PUPNoTranslationFoundIdentifier = @"PUPNoTranslationFoundIdentifier";
NSString * const PUPShowNonLocalizedStrings = @"NSShowNonLocalizedStrings";

static Puppeteer *sharedInstance = nil;



//********************************************

@interface Puppeteer()
{
    NSString * _bundlePath;
}
@end






//********************************************

@implementation Puppeteer



#pragma mark - Singleton & Initialization


//--------------------------------------------

+ (id)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}






#pragma mark - Localize method


//--------------------------------------------

+ (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName bundle:(NSBundle *)bundle
{
    NSBundle *puBundle = [[Puppeteer sharedInstance] getBundle];
    
    // do we have a bundle
    if (puBundle)
    {
        // try and get something from the puppeteer bundle
        NSString* localizedString = NSLocalizedStringWithDefaultValue(key, nil, puBundle, PUPNoTranslationFoundIdentifier, nil);
        
        
        // did we find something
        if (![localizedString isEqualToString:(NSString*)PUPNoTranslationFoundIdentifier]) {
            return localizedString;
        }
        
        
        // log the fail is NSShowNonLocalizedStrings is enabled
        else if ([[[NSUserDefaults standardUserDefaults] valueForKey:PUPShowNonLocalizedStrings] boolValue])
        {
            NSLog(@"Key doesn't exist in Puppeteer Bundle '%@'",key);
        }
    }
    
    
    // default bundle if not specified
    if (!bundle) {
        bundle = [NSBundle mainBundle];
    }
    
    
    // normal translation
    return NSLocalizedStringWithDefaultValue(key, tableName, bundle, value, nil);
}





#pragma mark - Bundle methods


//--------------------------------------------

-(NSBundle*)getBundle
{
    if (!_bundlePath) return nil;
    NSBundle *bundle = [NSBundle bundleWithPath:_bundlePath];
    return bundle;
}

//--------------------------------------------

- (void)setBundleWithRemoteUrl:(NSURL *)url
{
    [[Puppeteer sharedInstance] setBundleWithTempFilePath:url completionHandler:nil];
}

//--------------------------------------------

- (void)setBundleWithRemoteUrl:(NSURL *)url completionHandler:(void (^)(NSError *error))callback
{
    [[Puppeteer sharedInstance] downloadBundle:url completionHandler:^(NSError *error) {
        if (callback) {
            callback(error);
        }
    }];
}

//--------------------------------------------

- (void)setBundleWithTempFilePath:(NSString *)filePath
{
    [self setBundleWithTempFilePath:filePath completionHandler:nil];
}
    
//--------------------------------------------

- (void)setBundleWithTempFilePath:(NSString *)filePath completionHandler:(void (^)(NSError *error))callback
{
    _bundlePath = filePath;
}

//--------------------------------------------

- (BOOL)deleteBundle
{
    // return result
    BOOL result = [[NSFileManager defaultManager] removeItemAtPath:_bundlePath error:nil];
    
    _bundlePath = nil;
    
    return result;
}





#pragma mark - Network Methods


//--------------------------------------------

- (void)downloadBundle:(NSURL*)url completionHandler:(void (^)(NSError *error))callback
{
    NSURLSessionDownloadTask *downloadTask = [[NSURLSession sharedSession] downloadTaskWithURL:url
                                                        completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                            
                                                            if (!error) {
                                                                NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                                                                NSURL *documentsDirectoryURL = [NSURL fileURLWithPath:documentsPath];
                                                                NSURL *tmpPath = [documentsDirectoryURL URLByAppendingPathComponent:[[response URL] lastPathComponent]];
                                                                NSLog(@"tmpPath: %@",tmpPath);
                                                                
                                                                _bundlePath = [[documentsDirectoryURL URLByAppendingPathComponent:PUPBundleIdentifier] absoluteString];
                                                                NSLog(@"_bundlePath: %@",_bundlePath);
                                                            }
                                                            
                                                            else{
                                                                NSLog(@"ERROR: %@",error);
                                                            }
                                                            
                                                            callback(error);
                                                        }];
    [downloadTask resume];
}






#pragma mark - Other Methods


//--------------------------------------------

// Notify when shit is up
+ (void)puppeteerReady
{
    [[NSNotificationCenter defaultCenter] postNotificationName:PUPNofictionIdentifier object:self];
}

//--------------------------------------------

+ (void)log:(BOOL)isLogging
{
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:isLogging] forKey:PUPShowNonLocalizedStrings];
}



@end
